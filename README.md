1. Installl dependencies:
 
	    composer install

2. Create "twitter.local.php" with the from the following skeleton:

	    <?php
	    // Credentials from https://apps.twitter.com/app
	
	    define('CONSUMER_KEY', '');
	    define('CONSUMER_SECRET', '');
	    define('ACCESS_TOKEN', '');
	    define('ACCESS_TOKEN_SECRET', '');
	    
	    define('TREND_LOCATION', 'Houston');
	    ?> 


