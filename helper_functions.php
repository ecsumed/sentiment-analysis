<?php 
	require("vendor/autoload.php");
	use predictionio\EngineClient;
	$engineClient = new EngineClient('http://localhost:8000');

	function getColor($var)
	{
	    $val = round($var, 5);
	    if ($val >= 1.5 && $val < 2.5)
		return '#808080';
	    else if ($val >= 2.5)
		return '#008000';
	    else if ($val < 1.5)
		return '#FF0000';
	}
	function getSentiment($var)
	{
	    $val = round($var, 5);
	    if ($val >= 1.5 && $val < 2.5)
		return 'Neutral';
	    else if ($val >= 2.5)
		return 'Positive';
	    else if ($val < 1.5)
		return 'Negative';
	}
?>
