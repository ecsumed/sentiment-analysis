<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
   <HEAD>
      <TITLE>My first HTML document</TITLE>
		<?php 
			require("vendor/autoload.php");
			require 'helper_functions.php';
			require "twitter.local.php";
			include "menu.php";
			use Abraham\TwitterOAuth\TwitterOAuth;
		?>
   </HEAD>
   <BODY>
		<?php 
			$toa = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
			$places = $toa->get('trends/available');
	
			# use World id if selecte location trends not found	
			$woeid = '1';	
			$loc = 'World';
			foreach ($places as $place) { 
				if ($place->name == TREND_LOCATION) {
					# Replace world id with selected location id if location found		
					$woeid = $place->woeid;
					$loc = "$place->name, $place->country";
				}
			}
			$trends2 = $toa->get('trends/place', ["id" => $woeid])[0]->trends;
			$trends = array_slice($trends2, 0, 10);
				
			$cloudways_tweets = $toa->get('search/tweets', ["q" => "%23cloudways", "count" => 50]);	
			$sentiments = [];
			foreach ($cloudways_tweets->statuses as $status) { 
				# Calculate sentiment of tweets from #cloudways
				$response = $engineClient->sendQuery(array('s'=>$status->text));
				$sentiment = $response["sentiment"];
				
				# Push the sentiment in an array, so that we can calulate the average
				array_push($sentiments, $sentiment);
			}
			if (count($sentiments) <= 0) {
				continue;
			}
			$cw_avg = array_sum($sentiments) / count($sentiments);
		?>
			
		<h2><?php echo "#Cloudway Average"; ?></h2>
		<h2 style="color:<?php echo getColor($cw_avg); ?>"><?php echo $cw_avg; ?></h2>
		<h2 style="color:<?php echo getColor($cw_avg); ?>"><?php echo getSentiment($cw_avg); ?></h2>

		<h4><?php $date = date('Y-m-d H:i:s'); echo "As on $date"; ?></h2>
		<h1><?php echo "Top Trends for location: $loc"; ?></h1>
		<table class="tg">
			<?php foreach ($trends as $x=>$trend) { 
				# Get tweets for the trend
				$search_results = $toa->get('search/tweets', ["q" => $trend->query, "count" => 20]);	
	
				$tweets_sentiments = [];
				# extract tweet text from results
				foreach ($search_results->statuses as $status) { 
					# Calculate sentiment of tweet
					$response = $engineClient->sendQuery(array('s'=>$status->text));
					$sentiment = $response["sentiment"];
					
					# Push the sentiment in an array, so that we can calulate the average
					array_push($tweets_sentiments, $sentiment);
				}
				# Get average of sentiment of tweets
				if (count($tweets_sentiments) <= 0) {
					continue;
				}
				$avg = array_sum($tweets_sentiments) / count($tweets_sentiments);
			?>
			  <tr>
			    <th class="tg-c57o"><?php echo $x + 1; echo ".$trend->name"; ?></th>
			    <th style="color:<?php echo getColor($avg); ?>" class="tg-c57o"><?php echo $avg; ?></th>
			    <th style="color:<?php echo getColor($avg); ?>" class="tg-c57o"><?php echo getSentiment($avg); ?></th>
			  </tr>
			<?php } ?>
		</table>

   </BODY>
</HTML>

